
;; TODO list
;; 1. support editorconfig

;; for win
;;setenv(setenv "HOME" "E:/users/yuhai")
;;setenv(setenv "PATH" "E:/users/yuhai")

;;set the default file path
(setq default-directory "~/")
(add-to-list 'load-path "~/.emacs.d/site-lisp")

;; save last emacs status (1 | 0)
(desktop-save-mode 0)

;; 一打开就起用 text 模式。
(setq default-major-mode 'text-mode)
;; 语法高亮
(global-font-lock-mode t)

;; 支持emacs和外部程序的粘贴
(setq x-select-enable-clipboard t)

;; 默认显示 80列就换行
(setq default-fill-column 80)

;; 显示括号匹配
(show-paren-mode t)
(setq show-paren-style 'parentheses)

;; 关闭自动换行
(set-default 'truncate-lines nil)

;; 显示行号
;;(global-linum-mode t)

;; 空格替换TAB
;;(setq-default indent-tabs-mode nil)
(setq default-tab-width 4)
;;(setq tab-width 4)
(setq c-basic-offset 4)
(setq tab-stop-list ())

;;关闭烦人的出错时的提示声。
(show-paren-mode t)

;; 删除报警
(setq kill-ring-max 200)

;;让Emacs可以直接打开和显示图片。
(auto-image-file-mode)


;; For my language code setting (UTF-8)
(setq current-language-environment "UTF-8")
;; (setq default-input-method "chinese-py")
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)


;; 解决中文文件名乱码
;;(prefer-coding-system 'utf-8)
;;(setq file-name-coding-system 'gbk) ;; for win ？？？ TODO 兼容

;; 字体
;; Setting English Font
;;(set-face-attribute
;;  'default nil :font "Consolas 9")
;; Chinese Font
;;(dolist (charset '(kana han symbol cjk-misc bopomofo))
;;    (set-fontset-font (frame-parameter nil 'font)
;;                      charset
;;                      (font-spec :family "Microsoft Yahei" :size 12)))


;; 鼠标滚轮放大缩小字体
;; ??? TODO 兼容
;; For Linux
(global-set-key (kbd "<C-mouse-4>") 'text-scale-increase)
(global-set-key (kbd "<C-mouse-5>") 'text-scale-decrease)
 
;; For Windows
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)



;; 自定义:删除当前行
(global-set-key (kbd "C-x <delete>") 'kill-whole-line)
;;(global-set-key (kbd "C- d") 'kill-whole-line)

;; 透明
(modify-frame-parameters (selected-frame) `((alpha . 90)))

;;关闭出错时的提示声
(setq visible-bell t)

;; 不自动保存模式
(setq auto-save-mode nil)

;; 自动的在文件末增加一新行
;;(setq require-final-newline t)

;;复制一行
(defun copy-line (&optional arg)
"Save current line into Kill-Ring without mark the line "
(interactive "P")
(copy-thing 'beginning-of-line 'end-of-line arg)
(paste-to-mark arg))
;;绑定键
(global-set-key (kbd "C-c l") (quote copy-line))

;; 自动备份设置
(setq
    backup-by-copying t ; 自动备份
    backup-directory-alist
    '(("." . "~/.emacs.d/.saves")) ; 自动备份在目录"~/.saves"下
    delete-old-versions t ; 自动删除旧的备份文件
    kept-new-versions 6 ; 保留最近的6个备份文件
    kept-old-versions 2 ; 保留最早的2个备份文件
    version-control t) ; 多次备份








;; 包管理
(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/"))
(package-initialize)

;; php-mode markdown-mode
;; monokai-theme solarized-theme

;;php-mode
(autoload 'php-mode "php-mode.el" 
    "Major mode for editing php files" t)
(setq auto-mode-alist
    (cons '("\\.php" . php-mode) auto-mode-alist))

;; markdown-mode
(autoload 'markdown-mode "markdown-mode.el"
    "Major mode for editing Markdown files" t)
(setq auto-mode-alist
    (cons '("\\.md" . markdown-mode) auto-mode-alist))

(load-theme 'monokai t)