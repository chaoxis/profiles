;;(prelude-require-packages '(all-the-icons neotree caroline-theme))

;; This is only needed once, near the top of the file
(eval-when-compile
    ;; Following line is not needed if use-package.el is in ~/.emacs.d
    ;; (add-to-list 'load-path "<path where use-package is installed>")
    (require 'use-package))

(menu-bar-mode 0)
(tool-bar-mode t)
(if (display-graphic-p) (scroll-bar-mode 0))

;; theme
;;(load-theme 'manoj-dark t)
;; available themes:
;; adwaita caroline firecode leuven manoj-dark wombat zenburn
;;(load-theme 'caroline t)


;;(setq mac-command-modifier 'meta)
;;(setq mac-option-modifier 'super)


;; shortcuts
(global-set-key "\C-xk" 'kill-this-buffer)
(global-set-key "\C-z" 'set-mark-command)
(global-set-key "\C-u" 'er/expand-region)

(global-set-key [(meta g)] 'goto-line)

(global-set-key (kbd "M-1") 'delete-other-windows)
(global-set-key (kbd "M-2") 'split-window-vertically)
(global-set-key (kbd "M-3") 'split-window-horizontally)
(global-set-key (kbd "M-0") 'delete-window)

(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(global-set-key "\C-c\ [" 'shrink-window-horizontally)
(global-set-key "\C-c\ ]" 'enlarge-window-horizontally)


;; org-mode config
(setq org-src-fontify-natively t) ;; set org-mode src highlight

(use-package all-the-icons)

;;(require 'powerline)
;;(powerline-default-theme)
;;(require 'spaceline-config)
;;(spaceline-spacemacs-theme)

;;(use-package spaceline-all-the-icons
;;    :after spaceline
;;    :config (spaceline-all-the-icons-theme))

(require 'neotree)
(global-set-key [f8] 'neotree-toggle)

(setq neo-theme (if (display-graphic-p) 'icons 'arrow))

(setq neo-window-width 40
    neo-create-file-auto-open t
    ;;neo-banner-message "Press ? for neotree help"
    neo-show-updir-line nil
    neo-mode-line-type 'neotree
    neo-smart-open t
    neo-dont-be-alone t
    neo-persist-show nil
    neo-show-hidden-files t
    neo-auto-indent-point t
    neo-modern-sidebar t
    neo-vc-integration nil)
