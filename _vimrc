" Vundle need
filetype off
set rtp=~/.vim,$VIMRUNTIME,~/.vim/bundle/vundle
call vundle#rc()
" Github repos
Bundle 'gmarik/vundle'
Bundle 'plasticboy/vim-markdown'
Bundle 'hsitz/VimOrganizer'
Bundle 'Lokaltog/vim-powerline'

" vim-scripts repos
" Bundle 'FuzzyFinder

" non github repos
" Bundle 'git://git.wincent.com/command-t.git

" Vundle over turn back filetype settings
filetype plugin indent on

" VimOrganizer setup
let g:ft_ignore_pat = '.org'
au! BufRead,BufWrite,BufWritePost,BufNewFile *.org
au BufEnter *.org call org#SetOrgFileType()
command! OrgCapture :call org#CaptureBuffer()
command! OrgCaptureFile :call org#OpenCaptureFile()
" end VimOrganizer setup

set t_Co=256

"不模拟VI的按键
set nocompatible
"自动语法高亮
syntax on
"禁止嘀嘀声音
set noerrorbells
"显示行号
set number


"合并状态栏和命令栏
set laststatus=2 "总是显示状态栏

"屏蔽启动那个索马里儿童提示
set shortmess=atl

"支持UTF-8
set fileencodings=utf8,ucs-bom,chinese,cp936
set encoding=utf8

" vim-powerline settings
let g:Powerline_symbols = 'unicode' "使界面不乱码

" 配色方案
"colorscheme desert

" 把molokai背景色做成偏黄
if !exists("g:molokai_original")
  let g:molokai_original = 1
endif
colorscheme molokai

" 禁止生成备份文件.
set nobackup
set nowritebackup

" 图形启动时的设置
if has ("gui_running")
  set columns=130
  set lines=50
  "隐藏菜单
  "set guioptions-=m
  "隐藏工具栏
  "set guioptions-=T
  "显示tabline 0 永远不显示 1 两个以上显示 2 永远显示
  set showtabline=1
  "显示底部滚动条
  "set guioptions+=b
endif



"文本表格
map <S-Tab> :call NextField(' \{2,}',2,' ',0)<CR>
map! <S-Tab> <C-O>:call NextField(' \{2,}',2,' ',0)<CR>
" function: NextField
" Args: fieldsep,minlensep,padstr,offset
"
" NextField checks the line above for field separators and moves the cursor on
" the current line to the next field. The default field separator is two or more
" spaces. NextField also needs the minimum length of the field separator,
" which is two in this case. If NextField is called on the first line or on a
" line that does not have any field separators above it the function echoes an
" error message and does nothing.

func! NextField(fieldsep,minlensep,padstr,offset)
    let curposn = col(".")
    let linenum = line(".")
    let prevline = getline(linenum-1)
    let curline = getline(linenum)
    let nextposn = matchend(prevline,a:fieldsep,curposn-a:minlensep)+1
    let padding = ""

    if nextposn > strlen(prevline) || linenum == 1 || nextposn == 0
        echo "last field or no fields on line above"
        return
    endif

    echo ""

    if nextposn > strlen(curline)
        if &modifiable == 0
             return
        endif
        let i = strlen(curline)
        while i < nextposn - 1
            let i = i + 1
            let padding = padding . a:padstr
        endwhile
        call setline(linenum,substitute(curline,"$",padding,""))
    endif
    call cursor(linenum,nextposn+a:offset)
    return
endfunc
"END 文本表格

"括号自动补全
:inoremap { {}<ESC>i
:inoremap } <c-r>=UClosePair('}')<CR>
:inoremap ( ()<ESC>i
:inoremap ) <c-r>=UClosePair(')')<CR>

func! UClosePair(char)
  if getline('.')[col('.') - 1] == a:char
      return "\<Right>"
  else
      return a:char
  endif
endfunc
"END 括号自动补全

" 空格代替Tab
set smarttab "根据文件中其他地方的缩进空格个数来定一个Tab是多少空格
set tabstop=4 "一个Tab是多少个空格
set shiftwidth=4 "每级缩进是多少个空格
set expandtab "将Tab扩展成空格


" 自动缩进
set ai

" 设置Vim的历史记录可以记忆的行数
set history=100

" 自动识别Unix和Dos文件格式
" set fileformats=unix,dos,mac

" 当_vimrc文件被修改时，自动载入
autocmd! bufwritepost .vimrc source %

"NERD Tree
"let NERDChristmasTree=1
"let NERDTreeAutoCenter=1
"let NERDTreeBookmarksFile=$VIM.'\Data\NerdBookmarks.txt'
"let NERDTreeMouseMode=2
"let NERDTreeShowBookmarks=1
"let NERDTreeShowFiles=1
"let NERDTreeShowHidden=0
"let NERDTreeShowLineNumbers=1
"let NERDTreeWinPos='left'
"let NERDTreeWinSize=31
"nnoremap <F5> :NERDTreeToggle<CR>
"inoremap <F5> <Esc>:NERDTreeToggle<CR>
