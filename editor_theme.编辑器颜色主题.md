## GeenceDark
RGB

**Editor**
- Background: #1e2029
              30 32 41  
- Current line highlight: #343745
                          52 55 69
- Foreground: #ffffff
              255 255 255
              
**Comment**
- Comment: #00bc3c
           0 188 60

**Base**
- Keyword exclude return: #d1008b
           209 0 139
- Return   #
           255 0 128
**Operator&Sign**
- Operators =<>
- Semicolon ;
- Dot .
- Comma ,
- Braces {}
- Parentheses ()
           
**Type**
- Number: #7c69c9
          124 105 201
- String: #ff1331
          225 19 49

**Variable**
- Parameter: #FD971F
             253 151 31
- Local Variable: #FFB35B
                 
- Global micro: #d18f5b
                209 143 91
- Constant: #68878f / 104 135 143

**Class/Interface**
- Class: #00FFE8 Bold
         0 255 232 
- Abstract Class:
- Interface: #24AC7A Bold
             36 172 122
- Enum:



**Field**
- Instance Field: #9876AA
                  152 118 170
- Static Field: #9876AA
                152 118 170 Italic


**Method/Function**
- Method/Function Declaration:#FFC66D
                              255 198 109

- Function Call:
- Instance Method Call:
- Static Method Call:  Italic

- Abstract Method:
- Inherited Method:


- Constructor call:
- Constructor declaration:


**Document comment**
- Document Comment: #547797
                    84 119 151
- Document Tag:
- Document Tag Value:
- Document Markup:


**Others**

- Url: #2222da



## 语言特别部分

**Java**
- Annotation Name:#BBB529
                  187 181 41
- Annotation Attribute Name:#BBB529
                            187 181 41
- Anonymous Class
- Type Parameter

**PHP**
- Predefined Symbols

- Heredoc ID
- Heredoc Content

- PHP Tag


--------------------------------------


暗绿 0 80 50


##Geence
- Background: #
- Foreground: #
- Comment: #008000
- Keyword: #0000e1
- Core functions: #e10000
- Variable: #008080
- String: #ff00ff
- Number: #800080
- Attribute: #808000
